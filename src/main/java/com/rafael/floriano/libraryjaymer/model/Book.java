package com.rafael.floriano.libraryjaymer.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@Builder
public class Book {

    private Long idBook;

    private String titulo;

    private String autor;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date publishedDate;

    private String editor;

}
