package com.rafael.floriano.libraryjaymer.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class User {
    private Long idUser;
    private String name;
    private String cpf;
    private String login;
    private String password;
}
