package com.rafael.floriano.libraryjaymer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibraryJaymerApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibraryJaymerApplication.class, args);
	}

}
