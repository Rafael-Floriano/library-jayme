package com.rafael.floriano.libraryjaymer.service;

import com.rafael.floriano.libraryjaymer.model.User;
import com.rafael.floriano.libraryjaymer.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Transactional(rollbackFor = Exception.class)
    public void saveNewUser(User user) {
        try {
          userRepository.saveNewUser(user);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Transactional(readOnly = true)
    public List<User> showAllUsers() {
        List<User> userList = new ArrayList<>();
        try {
             userList = userRepository.showAllUsers();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return userList;
    }
}
