package com.rafael.floriano.libraryjaymer.service;

import com.rafael.floriano.libraryjaymer.model.Book;
import com.rafael.floriano.libraryjaymer.repository.BookRepository;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@NoArgsConstructor
public class BookService {
    @Autowired
    private BookRepository bookRepository;

    @Transactional(rollbackFor = Exception.class)
    public void saveNewBook(Book book) {
        try {
            bookRepository.saveNewBook(book);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Transactional(readOnly = true)
    public List<Book> showAllBooks() {
        List<Book> bookList = new ArrayList<>();
        try {
            bookList = bookRepository.showAllBooks();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return bookList;
    }
}
