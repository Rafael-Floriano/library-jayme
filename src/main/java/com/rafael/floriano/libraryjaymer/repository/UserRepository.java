package com.rafael.floriano.libraryjaymer.repository;

import com.rafael.floriano.libraryjaymer.model.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepository {

    private JdbcTemplate jdbcTemplate;

    public UserRepository(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    public void saveNewUser(User user) {
        String sql = "INSERT INTO library_user (name, cpf,login,password) " +
                " values (?,?,?,?)";
        jdbcTemplate.update(sql, user.getName(), user.getCpf(), user.getLogin(),user.getPassword());
    }

    public List<User> showAllUsers() {
        String sql = "SELECT * FROM library_user";
        List<User> userList = new ArrayList<>();
        jdbcTemplate.query(sql, (resultSet) -> {
            while (resultSet.next()) {
                userList.add(
                        User.builder().idUser(resultSet.getLong("user_id"))
                                .name(resultSet.getString("name"))
                                .cpf(resultSet.getString("cpf"))
                                .login(resultSet.getString("login"))
                                .password(resultSet.getString("password"))
                                .build());
            }
        });
        return userList;
    }
}
