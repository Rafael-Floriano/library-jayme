package com.rafael.floriano.libraryjaymer.repository;

import com.rafael.floriano.libraryjaymer.model.Book;
import com.rafael.floriano.libraryjaymer.model.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class BookRepository {
    private JdbcTemplate jdbcTemplate;

    public BookRepository(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void saveNewBook(Book book) {
        String sql = " INSERT INTO book (title, author, published_date, editor) " +
                " VALUES (?,?,?,?) ";
        jdbcTemplate.update(sql, book.getTitulo(), book.getAutor(), book.getPublishedDate(), book.getEditor());
    }

    public List<Book> showAllBooks() {
        String sql = "SELECT * FROM book";
        List<Book> bookList = new ArrayList<>();
        jdbcTemplate.query(sql, (resultSet) -> {
            while (resultSet.next()) {
                bookList.add(
                        Book.builder().idBook(resultSet.getLong("book_id"))
                                .titulo(resultSet.getString("title"))
                                .autor(resultSet.getString("author"))
                                .publishedDate(resultSet.getDate("published_date"))
                                .editor(resultSet.getString("editor")).build()
                );
            }
        });
        return bookList;
    }

}
