package com.rafael.floriano.libraryjaymer.controller;

import com.rafael.floriano.libraryjaymer.model.User;
import com.rafael.floriano.libraryjaymer.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/user/newUser")
    public String getFormNewUser() {
        return "userTemplates/newUser";
    }

    @PostMapping("/user/saveNewUser")
    public String postNewUser(User user) {
        userService.saveNewUser(user);
        return "redirect:/user/listAllUser";
    }

    @GetMapping("/user/listAllUser")
    public String listAllUser(Model model) {
        List<User> userList = userService.showAllUsers();
        model.addAttribute("users", userList);
        return "userTemplates/userList";
    }
}
