package com.rafael.floriano.libraryjaymer.controller;

import com.rafael.floriano.libraryjaymer.model.Book;
import com.rafael.floriano.libraryjaymer.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class BookController {
    @Autowired
    private BookService bookService;

    @GetMapping("/book/newBook")
    public String getFormNewBook() {
        return "bookTemplates/newBook";
    }

    @PostMapping("/book/saveNewBook")
    public String postNewBook(Book book) {
        bookService.saveNewBook(book);
        return "redirect:/book/listAllBook";
    }

    @GetMapping("/book/listAllBook")
    public String listAllUser(Model model) {
        List<Book> bookList = bookService.showAllBooks();
        model.addAttribute("books", bookList);
        return "bookTemplates/bookList";
    }
}
